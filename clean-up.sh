#!/usr/bin/env bash 
#
# Script name: clean-up.sh
# Description: Script for deleting everything not named "PKGBUILD".
# GitLab: https://www.gitlab.com/dwt1/dtos-pkgbuild
# Contributors: Derek Taylor

# Go into x86_64 sub-directory and get a list of files
# not named PKGBUILD and not named *.install. 
find x86_64 -type f -not -name PKGBUILD -not -name "*.install" -delete
find x86_64 -type d -name src -exec rm -rv {} +
find x86_64 -type d -name pkg -exec rm -rv {} +
